/*
 * @lc app=leetcode.cn id=2 lang=java
 *
 * [2] 两数相加
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode node = new ListNode(0);
        ListNode p = l1, q = l2, temp = node;
        int add = 0;
        while (p != null || q != null) {
            int x = (p != null) ? p.val : 0;
            int y = (q != null) ? q.val : 0;
            int sum = add + x + y;
            add = sum / 10;
            temp.next = new ListNode(sum % 10);
            temp = temp.next;
            if (p != null)
                p = p.next;
            if (q != null)
                q = q.next;
        }
        if (add > 0) {
            temp.next = new ListNode(add);
        }
        return node.next;
    }
}
// @lc code=end

