/*
 * @lc app=leetcode.cn id=7 lang=javascript
 *
 * [7] 整数反转
 */

// @lc code=start
/**
 * @param {number} x
 * @return {number}
 */
var reverse = function (x) {
  let rx = Math.abs(x).toString().split("").reverse().join("")

  if (x < 0) {
    return rx <= Math.pow(2, 31) ? -rx : 0;
  } else {
    return rx< Math.pow(2, 31) ? rx : 0;
  }

};
// @lc code=end

