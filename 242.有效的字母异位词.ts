/*
 * @lc app=leetcode.cn id=242 lang=typescript
 *
 * [242] 有效的字母异位词
 */

// @lc code=start
function isAnagram(s: string, t: string): boolean {
    const sMap = new Map();
    const tMap = new Map();

    for (let i = 0; i < s.length; i++) {
        const e = s[i];
        if (sMap.has(e)) {
            sMap.set(e, sMap.get(e) + 1)
        } else {
            sMap.set(e, 1)
        }
    }

    for (let i = 0; i < t.length; i++) {
        const e = t[i];
        if (tMap.has(e)) {
            tMap.set(e, tMap.get(e) + 1)
        } else {
            tMap.set(e, 1)
        }
    }
    if (tMap.size != sMap.size) {
        return false
    }
    for (let i of tMap.keys()) {
        if (tMap.get(i) != sMap.get(i)) {
            return false
        }
    }

    return true
};
// @lc code=end

