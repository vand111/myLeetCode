/*
 * @lc app=leetcode.cn id=14 lang=javascript
 *
 * [14] 最长公共前缀
 */

// @lc code=start
/**
 * @param {string[]} strs
 * @return {string}
 */
var longestCommonPrefix = function(strs) {
  if(strs.length==0){return ""}
  const l0 = strs[0];
  if(strs.length==1){return strs[0]||""}
  if(!l0){return ""}
  for(let i=1;i<=l0.length;i++){
    const ll = l0.slice(0,i);
    let flag = false
    strs.forEach((n,ii)=>{
      if(ll !=  n.slice(0,i)){
        flag = true
      }
    })
    if(flag){
      if(i==1){
        return ""
      }else{
        return l0.slice(0,i-1)
      }
    };
  }
  return l0;
};
// @lc code=end
// @after-stub-for-debug-begin
module.exports = longestCommonPrefix;
// @after-stub-for-debug-end