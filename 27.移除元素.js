/*
 * @lc app=leetcode.cn id=27 lang=javascript
 *
 * [27] 移除元素
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @param {number} val
 * @return {number}
 */
var removeElement = function(nums, val) {
  var a=0;
  var b =0;
  while(b<nums.length){
    if(nums[b]!=val){
      nums[a] =nums[b];
      a++
    }
    b++
  }
  
  return a
};
// @lc code=end

