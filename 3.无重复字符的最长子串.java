
/*
 * @lc app=leetcode.cn id=3 lang=java
 *
 * [3] 无重复字符的最长子串
 */
import java.util.HashSet;
import java.util.Set;

// @lc code=start
class Solution {
    public int lengthOfLongestSubstring(String s) {
        if(s.length() ==1){
            return 1;
        }
        Set<String> se = new HashSet<>();
        int lnum = 0;
        for (int a = 0; a < s.length(); a++) {
            for (int b = 0; b < s.length(); b++) {
                String sb = s.substring(b, b);
                if(se.contains(sb)){
                    se.add(sb);
                }else{
                    lnum = lnum>se.size()?lnum:se.size();
                    se.clear();
                    break;
                }
            }
        }
        return lnum;

    }
}
// @lc code=end

