/*
 * @lc app=leetcode.cn id=20 lang=javascript
 *
 * [20] 有效的括号
 */

// @lc code=start
/**
 * @param {string} s
 * @return {boolean}
 */
const leftL = ["(","[","{"];
const rightL= [")","]","}"]

var isValid = function(s) {
  let zan = [];

  for(let i=0;i<s.length;i++){
    const si = s[i]
    // 左边增加zan
    if(leftL.includes(si)){
      zan.push(si)
    }
    //右边减少 并判断
    if(rightL.includes(si)){
      let index = rightL.indexOf(si)
      if(zan[zan.length-1]==leftL[index]){
        zan.pop()
      }else{
        return false
      }
    }
  }
  if(zan.length==0){
    return true
  }
  return false


};
// @lc code=end


// @after-stub-for-debug-begin
module.exports = isValid;
// @after-stub-for-debug-end