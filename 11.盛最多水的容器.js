/*
 * @lc app=leetcode.cn id=11 lang=javascript
 *
 * [11] 盛最多水的容器
 */

// @lc code=start
/**
 * @param {number[]} height
 * @return {number}
 */
var maxArea = function(height) {
  const len = height.length;
  let mostWater = 0; //  记录最大盛水
  for(var q=0;q<len;q++){

    for(var p = len-1;p>q;p--){
      //计算两者容量  两者较小的 乘以距离
      const minHeight = height[p]>height[q]?height[q]:height[p];
      const rongji = minHeight *(p-q);
      mostWater = mostWater>rongji?mostWater:rongji;
    }

  }

  return mostWater;
};
// @lc code=end

