/*
 * @lc app=leetcode.cn id=56 lang=typescript
 *
 * [56] 合并区间
 */

// @lc code=start
function merge(intervals: number[][]): number[][] {
    let rList: number[][] = []
    let setItem = intervals[0]
    for (let i = 1; i < intervals.length; i++) {
        const n = intervals[i];

        if(n[0]<=setItem[0]&&setItem[0]<=n[1]){
            setItem[1] = Math.max(setItem[1],n[1])
            rList.push(setItem);
            continue;
        }
        if(n[0]<=setItem[1]&&setItem[1]<=n[1]){
            setItem[0] = Math.min(setItem[0],n[0])
            rList.push(setItem);
            continue;
        }

        rList.push(setItem);
        setItem = n;
        
    }
    rList.push(setItem);
    return rList
};
// @lc code=end

