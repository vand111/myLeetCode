/*
 * @lc app=leetcode.cn id=927 lang=javascript
 *
 * [927] 三等分
 */

// @lc code=start
/**
 * @param {number[]} A
 * @return {number[]}
 */
var threeEqualParts = function(A) {
  const len = A.length;
  for(var a=0;a<len-1;a++){

    for(var b =a+1;b<len-0;b++){
      var n1=[];
      var n2=[];
      var n3=[];
      A.forEach((n,i)=>{
        if(i<=a){
          n1.push(n);
        }
        if(i>a&&i<b){
          n2.push(n);
        }
        if(i>=b){
          n3.push(n);
        }
      });
      if(arr2num(n1)==arr2num(n2)&&arr2num(n2)==arr2num(n3)){
        return [a,b]
      }
    }

  }
  return [-1,-1]
};

//数组转为数字
/**
 * 
 * @param {number[]} nums 
 */
function arr2num(nums){
  if(!nums) {return false}
  var num = "";
  nums.forEach((n,i)=>{
    num+=n.toString()
  })
  return parseInt(num,2)
}
// @lc code=end


// @after-stub-for-debug-begin
module.exports = threeEqualParts;
// @after-stub-for-debug-end