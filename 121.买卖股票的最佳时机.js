/*
 * @lc app=leetcode.cn id=121 lang=javascript
 *
 * [121] 买卖股票的最佳时机
 */

// @lc code=start
/**
 * @param {number[]} prices
 * @return {number}
 */
var maxProfit = function (prices) {
  let profit = 0, min_price = prices[0];
  for (let i = 0; i < prices.length; ++i) {
    min_price = Math.min(min_price, prices[i]);
    profit = Math.max(profit, prices[i] - min_price);
  }
  return profit;
};
// @lc code=end

