/*
 * @lc app=leetcode.cn id=122 lang=typescript
 *
 * [122] 买卖股票的最佳时机 II
 */

// @lc code=start
function maxProfit(prices: number[]): number {
  let profit = 0;
  for (let i = 0; i < prices.length - 1; i++) {
    const tmp = prices[i + 1] - prices[i];
    if (tmp > 0) {
      profit += tmp
    }
  }
  return profit;
};
// @lc code=end

