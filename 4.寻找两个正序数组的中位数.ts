/*
 * @lc app=leetcode.cn id=4 lang=typescript
 *
 * [4] 寻找两个正序数组的中位数
 */

// @lc code=start
function findMedianSortedArrays(nums1: number[], nums2: number[]): number {
    // 合并
    let list = [...nums1, ...nums2].sort((a, b) => a - b);
    // while(nums1.length>0||nums2.length>0){
    //     if(nums1[0]&&nums2[0]){
    //         if(nums1[0]<nums2[0]){
    //             list.push(nums1[0]);
    //             nums1.shift()
    //         }else{
    //             list.push(nums2[0]);
    //             nums2.shift()
    //         }
    //     }else{
    //         if(!nums1[0]){
    //             list.push(nums2[0]);
    //             nums2.shift()
    //         }else{
    //             list.push(nums1[0]);
    //             nums1.shift()
    //         }
    //     }
    // }
    if (list.length % 2 == 0) {
        return (list[list.length / 2] + list[list.length / 2 - 1]) / 2
    }
    return list[(list.length - 1) / 2]
};
// @lc code=end

