/*
 * @lc app=leetcode.cn id=82 lang=javascript
 *
 * [82] 删除排序链表中的重复元素 II
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var deleteDuplicates = function (head) {
    if (!head) {
        return head
    }
    let p = new ListNode(0, head);
    let n = p;
    while (n.next && n.next.next) {
        if (n.next.val == n.next.next.val) {
            let v = n.next.next.val
            while (n.next && n.next.val == v) {
                n.next = n.next.next
            }
        } else {
            n = n.next
        }

    }

    return p.next
};
// @lc code=end

