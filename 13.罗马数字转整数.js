/*
 * @lc app=leetcode.cn id=13 lang=javascript
 *
 * [13] 罗马数字转整数
 */

// @lc code=start
/**
 * @param {string} s
 * @return {number}
 */
var romanToInt = function (s) {
  var sum = 0;
  const len = s.length;
  for (var a = 0; a < len; a++) {
    if (a < len - 1 && lm(s[a]) < lm(s[a + 1])) {
      sum -= lm(s[a])
    } else {
      sum += lm(s[a])
    }
  }
  return sum
};
// switch 并不比map快多少
function lm(ch) {
  switch (ch) {
    case 'I': return 1;
    case 'V': return 5;
    case 'X': return 10;
    case 'L': return 50;
    case 'C': return 100;
    case 'D': return 500;
    case 'M': return 1000;
    default: return 0;
  }
}

// const lm = {
//   "I": 1
//   , "V": 5
//   , 'X': 10
//   , 'L': 50
//   , 'C': 100
//   , 'D': 500
//   , 'M': 1000
// }
// @lc code=end

