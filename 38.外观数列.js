/*
 * @lc app=leetcode.cn id=38 lang=javascript
 *
 * [38] 外观数列
 */

// @lc code=start
/**
 * @param {number} n
 * @return {string}
 */
var countAndSay = function (n) {
    if (n == 1) {
        return "1"
    }
    if (n == 2) {
        return "11"
    }
    const c = countAndSay(n - 1);
    let p =  c;
    p+="a"
    // 分解数组
    let t = ""
    let list = [];

    let a= ""

    for (let index = 0; index < p.length - 1; index++) {
        const e = p[index];
        a += e.toString()
        if (e == p[index + 1]) {
            
        } else {
           list.push(a)
           a = ""
        }
    }
    list.forEach(n => {
        t += n.length + "" + n[0]
    })
    return t
};
// @lc code=end


// @after-stub-for-debug-begin
module.exports = countAndSay;
// @after-stub-for-debug-end