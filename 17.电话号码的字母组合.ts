/*
 * @lc app=leetcode.cn id=17 lang=typescript
 *
 * [17] 电话号码的字母组合
 */

// @lc code=start
function letterCombinations(digits: string): string[] {
  const dmap = {
    "2": ["a", "b", "c"],
    "3": ["d", "e", "f"],
    "4": ["g", "h", "i"],
    "5": ["j", "k", "l"],
    "6": ["m", "n", "o"],
    "7": ["p", "q", "r", "s"],
    "8": ["t", "u", "v"],
    "9": ["w", "x", "y", "z"]
  }
  // 列表插入一位
  function setL(l: string[], sl: string[]): string[] {
    if (l.length == 0) { return sl }
    // l.length = l.length * sl.length;
    let rl: string[] = []
    for (let i = 0; i < l.length; i++) {
      for (let j = 0; j < sl.length; j++) {
        // l[i * l.length + j] = l[i] + sl[j]
        rl.push(l[i] + sl[j])
      }
    }
    return rl;
  }

  let l: string[] = []
  if (digits == '') {
    return []
  }
  for (let i = 0; i < digits.length; i++) {
    l = setL(l, dmap[digits[i]])
  }

  return l;
};
// console.log(letterCombinations("2343445"))
// @lc code=end

