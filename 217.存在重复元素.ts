/*
 * @lc app=leetcode.cn id=217 lang=typescript
 *
 * [217] 存在重复元素
 */

// @lc code=start
function containsDuplicate(nums: number[]): boolean {
    nums.sort((a, b) => a - b)
    for (let index = 0; index < nums.length - 1; index++) {
        if (nums[index] == nums[index + 1]) {
            return true
        }
    }
    return false
};
// @lc code=end

