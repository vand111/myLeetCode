/*
 * @lc app=leetcode.cn id=136 lang=typescript
 *
 * [136] 只出现一次的数字
 */

// @lc code=start
function singleNumber(nums: number[]): number {
  let r = nums.sort();
  if (r[0] != r[1]) { return r[0] }
  for (let i = 1; i < r.length - 1; i++) {
    if (r[i] != r[i - 1] && r[i] != r[i + 1]) {
      return r[i]
    }
  }
  return r[r.length - 1]
};
// @lc code=end

