/*
 * @lc app=leetcode.cn id=9 lang=javascript
 *
 * [9] 回文数
 */

// @lc code=start
/**
 * @param {number} x
 * @return {boolean}
 */
var isPalindrome = function(x) {
  const xStr = x.toString()
  const len = xStr.length;
  for(let a=0;a<len/2;a++){
    if(xStr[a]!=xStr[len-1-a]){
      return false
    }
  }
  return true
};
// @lc code=end

