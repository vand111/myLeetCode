/*
 * @lc app=leetcode.cn id=1013 lang=javascript
 *
 * [1013] 将数组分成和相等的三个部分
 */

// @lc code=start
/**
 * @param {number[]} A
 * @return {boolean}
 */
var canThreePartsEqualSum = function(A) {
  const len = A.length;
  for(var a=0;a<len-2;a++){

    for(var b =a+1;b<len-1;b++){
      var n1=0;
      var n2=0;
      var n3=0;
      A.forEach((n,i)=>{
        if(i<=a){
          n1+=n;
        }
        if(i>a&&i<=b){
          n2+=n
        }
        if(i>b){
          n3+=n
        }
      });
      if(n1==n2&&n2==n3){
        return true
      }
    }

  }
  return false
};
// @lc code=end

