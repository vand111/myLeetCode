
import java.util.ArrayList;
import java.util.List;

/*
 * @lc app=leetcode.cn id=927 lang=java
 *
 * [927] 三等分
 */

// @lc code=start
class Solution {
    public int[] threeEqualParts(int[] A) {
        int len = A.length;
        for (int a = 0; a < len - 1; a++) {

            for (int b = a + 1; b < len - 0; b++) {
                List<Integer> n1 = new ArrayList<>();
                List<Integer> n2 = new ArrayList<>();
                List<Integer> n3 = new ArrayList<>();
                for (int i = 0; i < len; i++) {
                    int n = A[i];
                    if (i <= a) {
                        n1.add(n);
                    }
                    if (i > a && i < b) {
                        n2.add(n);
                    }
                    if (i >= b) {
                        n3.add(n);
                    }

                }
                if (n1.size() > 0 && n2.size() > 0 && n3.size() > 0) {
                    if (arr2num(n1) == arr2num(n2) && arr2num(n2) == arr2num(n3)) {
                        return new int[] { a, b };
                    }
                }
            }
        }
        return new int[] { -1, -1 };
    }

    public int arr2num(List<Integer> n1) {
        String num = "";
        for (int n : n1) {
            num += Integer.toString(n);
        }
        if ("".equals(num)) {
            return 0;
        }
        return Integer.parseInt(num, 2);
    }
}
// @lc code=end
