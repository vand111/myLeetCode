/*
 * @lc app=leetcode.cn id=56 lang=javascript
 *
 * [56] 合并区间
 */

// @lc code=start
/**
 * @param {number[][]} intervals
 * @return {number[][]}
 */
var merge = function (intervals) {
    let listSet = [];
    let rList = []
    intervals.forEach(n => {
        for (let i = n[0]; i <= n[1]; i++) {
            if (!listSet.includes(i)) {
                listSet.push(i)
            }
        }
    });
    listSet = listSet.sort((a, b) => a - b);
    let setItem = []
    for (let a = 0; a < listSet.length; a++) {
        const next = listSet[a + 1]
        const n = listSet[a]
        // if (!setItem[0]) {
        setItem[0] = setItem[0] ? setItem[0] : n
        if (!next) {
            setItem.push(n)
            rList.push(setItem)
            setItem = []
        } else {
            if (n != next - 1) {
                setItem.push(n)
                rList.push(setItem)
                setItem = []
            }
        }
        // }
    }
    return rList
};
// @lc code=end


// @after-stub-for-debug-begin
module.exports = merge;
// @after-stub-for-debug-end