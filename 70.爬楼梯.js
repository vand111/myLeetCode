/*
 * @lc app=leetcode.cn id=70 lang=javascript
 *
 * [70] 爬楼梯
 */

// @lc code=start
/**
 * @param {number} n
 * @return {number}
 */
var climbStairs = function(n) {
  var p=0;
  var q =0;
  var r=1;
  for(var i=1;i<=n;++i){
    p=q;
    q=r;
    r=q+p;
  }
  return r
  if(n==1){
    return 1;
  }
  if(n==2){
    return 2
  }
  return climbStairs(n-1)+climbStairs(n-2);
  
};
// @lc code=end

