/*
 * @lc app=leetcode.cn id=3 lang=javascript
 *
 * [3] 无重复字符的最长子串
 */

// @lc code=start
/**
 * @param {string} s
 * @return {number}
 */
var lengthOfLongestSubstring = function(s) {
  if(s.length==1){return 1}
  var se = new Set();
  var lnum = 0;
  for(var a=0;a<s.length;a++){
    for(var b=a;b<s.length;b++){
      if(!se.has(s[b])){
        se.add(s[b])
      }else{
        lnum= lnum>se.size?lnum:se.size;
        se.clear();
        break;
      }
    }
  }
  return lnum
};
// @lc code=end

