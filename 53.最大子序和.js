/*
 * @lc app=leetcode.cn id=53 lang=javascript
 *
 * [53] 最大子序和
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var maxSubArray = function (nums) {
  var res = nums[0];//
  var sum = 0;
  

  nums.forEach((n, i) => {
    if(sum>0){
      sum +=n
    }else{
      sum = n;
    }

    res = res>sum?res:sum;
  })

  return res;
};
// @lc code=end
