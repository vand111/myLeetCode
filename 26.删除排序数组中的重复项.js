/*
 * @lc app=leetcode.cn id=26 lang=javascript
 *
 * [26] 删除排序数组中的重复项
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var removeDuplicates = function(nums) {
  if(!nums){return []}
  var a = 0;
  for(let b =1;b<nums.length;b++){
    if(nums[b]!=nums[a]){
      nums[a+1] = nums[b];
      a++
    }
  }
  return a+1
  // return nums.slice(0,a+1).length;
  // return nums.filter((n,i)=>{return nums[i+1]?nums[i]!=nums[i+1]:false})
};
// @lc code=end


// @after-stub-for-debug-begin
module.exports = removeDuplicates;
// @after-stub-for-debug-end