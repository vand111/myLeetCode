/*
 * @lc app=leetcode.cn id=88 lang=typescript
 *
 * [88] 合并两个有序数组
 */

// @lc code=start
/**
 Do not return anything, modify nums1 in-place instead.
 */
function merge(nums1: number[], m: number, nums2: number[], n: number): void {
  // 先将nums2合并至nums1中
  for (let i = 0; i < n; i++) {
    nums1[m] = nums2[i];
    m++;
  }
  // 再对nums1进行合并
  nums1.sort((a, b) => a - b);
  // return nums1;

};
// @lc code=end

