/*
 * @lc app=leetcode.cn id=58 lang=typescript
 *
 * [58] 最后一个单词的长度
 */

// @lc code=start
function lengthOfLastWord(s: string): number {
    const list = s.split(" ");
    let len = list.length -1;
    let a = ""
    while(len>=0&&!a){
        a=list[len];
        len--;
    }
    return a.length
};
// @lc code=end

