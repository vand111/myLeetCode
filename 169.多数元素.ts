/*
 * @lc app=leetcode.cn id=169 lang=typescript
 *
 * [169] 多数元素
 */

// @lc code=start
function majorityElement(nums: number[]): number {
  const m: Map<number, number> = new Map();
  if(nums.length==1){return nums[0]}
  for (let i = 0; i < nums.length; i++) {
    const e = nums[i];
    if (!m.has(e)) {
      m.set(e, 1)
    } else {
      m.set(e, m.get(e)||0 + 1);
      if (m.get(e)||0 > nums.length / 2) {
        return e
      }
    }
  }
  return 0
};
// @lc code=end

