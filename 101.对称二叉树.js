/*
 * @lc app=leetcode.cn id=101 lang=javascript
 *
 * [101] 对称二叉树
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {boolean}
 */
var isSymmetric = function(root) {
  return check(root,root)  
};
/**
 * 
 * @param {TreeNode} q 
 * @param {TreeNode} p 
 * @return {boolean}
 */
function check(q,p){
  if(!q&&!p){return true}
  if(!q||!p){return false}
  return q.val===p.val&&check(q.left,p.right)&&check(q.right,p.left);
}

// @lc code=end

