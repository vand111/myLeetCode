/*
 * @lc app=leetcode.cn id=1137 lang=typescript
 *
 * [1137] 第 N 个泰波那契数
 */

// @lc code=start
function tribonacci(n: number): number {
    let list = [0,1,1];
    if(n>2){
        for(let i=3;i<=n;i++){
            list.push(list[i-1]+list[i-2]+list[i-3])
        }
    }
    return list[n]
};
// @lc code=end

