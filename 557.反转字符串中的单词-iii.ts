/*
 * @lc app=leetcode.cn id=557 lang=typescript
 *
 * [557] 反转字符串中的单词 III
 */

// @lc code=start
function reverseWords(s: string): string {
  const ss = s.split(" ");
  return ss.map(n=>{
    return n.split("").reverse().join("")
  }).join(" ")
};
// @lc code=end

