/*
 * @lc app=leetcode.cn id=28 lang=javascript
 *
 * [28] 实现 strStr()
 */

// @lc code=start
/**
 * @param {string} haystack
 * @param {string} needle
 * @return {number}
 */
var strStr = function(haystack, needle) {
  if(!needle){return 0}
  // return haystack.indexOf(needle)
  for(var a=0;a<haystack.length;a++){
    var flag = true
    for(var b=0;b<needle.length;b++){
      if(haystack[a+b]!=needle[b]){
        flag = false;
        break;
      }
    }
    if(flag){
      return a
    }
  }
  return -1
};
// @lc code=end

