/*
 * @lc app=leetcode.cn id=5 lang=javascript
 *
 * [5] 最长回文子串
 */

// @lc code=start
/**
 * @param {string} s
 * @return {string}
 */
var longestPalindrome = function(s) {
  if(!s.length||!s){return ""};
  if(s.length==1){return s}
  const len = s.length;
  let longStr = s[0]
  for(let start =0;start<len;start++){
    for(let end = len;end>start+1;end--){
      const sslice = s.slice(start,end)
      if(isPalindrome(sslice)){
        if(sslice.length>longStr.length){
          longStr = sslice
        }
      }
    }
  }
  return longStr
};
//判断是不是回文
/**
 * 
 * @param {string} s
 * @return {boolen} 
 */
let hasSet = new Set();
function isPalindrome(s){
  if(!s.length||!s){return false}
  if(hasSet.has(s)){
    return true
  }
  const len = s.length
  for(let i=0;i<len/2;i++){
    if(s[i]!=s[len-i-1]){
      return false
    }
  }
  hasSet.add(s)
  return true
}
/**
 * 
 * @param {string} s 
 * @param {number} q 
 * @param {number} p 
 */
function maxPalindrome(s,q,p){

  if(q<0&&p>=s.length){return 2};

  


  return maxPalindrome(s,q-1,p+1)+2;
}

// @lc code=end

