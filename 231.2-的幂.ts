/*
 * @lc app=leetcode.cn id=231 lang=typescript
 *
 * [231] 2的幂
 */

// @lc code=start
function isPowerOfTwo(n: number): boolean {
  if(n==1){
    return true
  }
  if(n<1){return false}
  while (!n.toString().includes(".")){
    n = n/2;
    if(n ==1 ){
      return true
    }
  }
  return false
};
// @lc code=end

