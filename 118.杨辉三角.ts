/*
 * @lc app=leetcode.cn id=118 lang=typescript
 *
 * [118] 杨辉三角
 */

// @lc code=start
function generate(numRows: number): number[][] {
    if(numRows==1){
        return [[1]]
    }
    if(numRows==2){
        return [[1],[1,1]]
    }
    let list = generate(numRows-1);
    let p = list[list.length-1];// 最后一个
    let pustItem:number[] = [];
    for(let i=0;i<numRows;i++){
        if(i==0||i==numRows-1){
            pustItem.push(1)
        }else{
            pustItem.push(p[i]+p[i-1])
        }
    }
    list.push(pustItem);
    return list
};
// @lc code=end

